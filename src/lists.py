emails = ['a@example.com', 'b@example.com']

print(len(emails))
print(emails[0])
print(emails[-1])

emails.append('cde@example.com')


# friend hobby
friend = {
    'name': 'Anonim',
    'age': 100,
    'hobbies': ['collect postcard', 'cycling']
}

print(friend['hobbies'][-1])

# temperature sum
temperature_january = [-4, 1.0, -7, 2]
temperature_february = [-13, -9, -3, 3]


def get_sum(list_with_numbers):
    return sum(list_with_numbers)


print(get_sum(temperature_january))
print(get_sum(temperature_february))
