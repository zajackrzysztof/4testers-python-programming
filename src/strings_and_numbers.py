first_name = 'Krzysztof'
last_name = 'Zając'
email = 'fake.email@gmail.com'

# F-string
print(f'Moje imię to {first_name},\nMoje nazwisko to {last_name}.\nMój email to {email}.')

print(f'Wynik operacji mnożenia liczb 4 i 5 to {4 * 5}')

# Algebra

circle_radious = 4
area_of_a_circle_with_radius = 3.14 * circle_radious ** 2
circumference_of_a_circle_with_radius = 2 * 3.14 * circle_radious

print(f'Area of a circle with radius {circle_radious}: {area_of_a_circle_with_radius}')
print(f'Circumference of a circle with radius {circle_radious}: {circumference_of_a_circle_with_radius}')
