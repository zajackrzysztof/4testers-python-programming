from cars import get_country_of_a_car_brand


def test_get_country_for_a_japanese_car():
    assert get_country_of_a_car_brand('Toyota') == 'Japan'


def test_get_country_for_a_japanese_car_lowercase():
    assert get_country_of_a_car_brand('toyota') == 'Japan'


def test_get_country_for_a_german_car():
    assert get_country_of_a_car_brand('audi') == 'Germany'


def test_get_country_for_a_france_car():
    assert get_country_of_a_car_brand('renault') == 'France'


def test_get_country_for_a_italian_car():
    assert get_country_of_a_car_brand('fiat') == 'Unknown'
