def print_name_and_city(name, city):
    print(f'Witaj {name.capitalize()}! Miło Cię widzieć w naszym mieście: {city.capitalize()}')


print_name_and_city('Michał', 'Toruń')
print_name_and_city('Beata', 'Gdynia')


def get_email(name, surname):
    return f'{name.lower()}.{surname.lower()}@4testers.pl'


print(get_email("Janusz", "Nowak"))
print(get_email("Barbara", "Kowalska"))
