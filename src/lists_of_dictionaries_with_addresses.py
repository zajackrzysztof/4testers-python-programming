if __name__ == '__main__':
    addresses = [
        {
            'city': 'Cracow',
            'street': 'Bracka',
            'house_number': '6',
            'post_code': '44-444'
        },
        {
            'city': 'Warsaw',
            'street': 'Długa',
            'house_number': '4',
            'post_code': '33-333'
        },
        {
            'city': 'Nowy Targ',
            'street': 'Krótka',
            'house_number': '9',
            'post_code': '55-555'
        }
    ]

    print(addresses[-1]['post_code'])
    print(addresses[1]['city'])
    addresses[0]['street'] = 'Słomiana'
    print(addresses)
