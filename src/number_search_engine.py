def find_divisible_subset(number_from, number_to, divisible_by):
    divisible_subset = []
    for number in range(number_from, number_to + 1):
        if number % divisible_by == 0:
            divisible_subset.append(number)
    return divisible_subset


if __name__ == '__main__':
    # range_and_factor = [1, 50, 9]
    print(find_divisible_subset(1, 50, 9))
    print(find_divisible_subset(-20, 25, 5))
