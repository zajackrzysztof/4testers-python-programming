import random
from datetime import date

female_names = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_names = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
last_names = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']
current_year = date.today().year


def generate_dictionary_with_random_personal_data(is_woman):
    first_name = random.choice(female_names) if is_woman else random.choice(male_names)
    last_name = random.choice(last_names)
    country = random.choice(countries)
    email = f'{first_name.lower()}.{last_name.lower()}@example.com'
    age = random.randint(5, 45)
    adult = age >= 18
    birth_year = current_year - age
    return {
        'first_name': first_name,
        'last_name': last_name,
        'country': country,
        'email': email,
        'age': age,
        'adult': adult,
        'birth_year': birth_year
    }


def generate_list_of_dictionaries_with_random_personal_data(people_number):
    list_of_dictionaries = []
    for number in range(people_number):
        is_woman = True if number % 2 == 0 else False
        list_of_dictionaries.append(generate_dictionary_with_random_personal_data(is_woman))
    return list_of_dictionaries


def print_personal_introduction_sentences(list_of_dictionaries):
    for person in list_of_dictionaries:
        print(f'Hi! I\'m {person["first_name"]} {person["last_name"]}. I come from {person["country"]} and I was '
              f'born in {person["birth_year"]}')


if __name__ == '__main__':
    list_of_dictionaries_with_random_personal_data = generate_list_of_dictionaries_with_random_personal_data(10)
    print(list_of_dictionaries_with_random_personal_data)
    print_personal_introduction_sentences(list_of_dictionaries_with_random_personal_data)
