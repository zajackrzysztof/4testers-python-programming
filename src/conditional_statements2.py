def check_if_normal_atmospheric_conditions(temperature_in_celsius, pressure):
    if temperature_in_celsius == 0 and pressure == 1013:
        return True
    else:
        return False


def is_it_japanese_car_brand(car_brand):
    if car_brand in ['Toyota', 'Suzuki', 'Mazda']:
        return True
    else:
        return False


if __name__ == '__main__':
    print(check_if_normal_atmospheric_conditions(0, 1013))
    print(check_if_normal_atmospheric_conditions(1, 1013))
    print(check_if_normal_atmospheric_conditions(0, 1014))
    print(check_if_normal_atmospheric_conditions(1, 1014))

    print(is_it_japanese_car_brand('Fiat'))
    print(is_it_japanese_car_brand('KIA'))
    print(is_it_japanese_car_brand('Toyota'))
