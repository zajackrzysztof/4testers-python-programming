my_name = 'Krzysztof'
age = 32
my_email = 'fake.email@gmail.com'

print(my_name)
print(age)
print(my_email)

# The description of my friend
friend_name = 'Gal Anonim'
friend_age = '100'
friend_animal_number = 0
friend_has_driving_licence = True
friendship_time_in_years = 8.5

print('Friend name -', friend_name)
print(friend_age, friend_animal_number, friend_has_driving_licence, friendship_time_in_years, sep='\n')
